#!/bin/ksh
#
# Basic (Poor man's) metadata agent!
#
# Instructions: Replace value of $USERACCOUNT with the standard wheel-group user used in your image
#               Script expects to be run from /usr/local/bin
#
# Copyright (c) 2019, Henry Bonath (henry@thebonaths.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

USERACCOUNT='openbsd'

export MYPATH="/usr/local/bin/openbsd-metadata.sh"
export USERUID=$(getent passwd $USERACCOUNT | cut -d: -f3)
export USERGID=$(getent passwd $USERACCOUNT | cut -d: -f4)
export USERREALNAME=$(getent passwd $USERACCOUNT | cut -d: -f5)
export USERHOME=$(getent passwd $USERACCOUNT | cut -d: -f6)
export USERSHELL=$(getent passwd $USERACCOUNT | cut -d: -f7)
export KEYPATH=$USERHOME/.ssh/authorized_keys
export ROOTKEYPATH="/root/.ssh/authorized_keys"

####################################################################################################

case $1 in
  execute)
    export SSHKEY=$(ftp -V -M -o - http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key/)
    export MYHOSTNAME=$(ftp -V -M -o - http://169.254.169.254/latest/meta-data/hostname/)
    if [ -n "$SSHKEY" ]; then
      if [[ $SSHKEY == ssh-rsa* ]] || [[ $SSHKEY == ecdsa* ]]; then
        echo "Metadata Service SSH Key found: " $SSHKEY
        echo $SSHKEY >> $KEYPATH
        chown $USERUID:$USERGID $KEYPATH
        chmod 0600 $KEYPATH
        echo $SSHKEY >> $ROOTKEYPATH
        chmod 0600 $ROOTKEYPATH
        echo "SSH Key Installed for users root and" $USERACCOUNT
        echo "Configuring doas for user $USERACCOUNT"
        echo "permit nopass keepenv $USERACCOUNT" >> /etc/doas.conf
        chmod 0600 /etc/doas.conf
      else
        echo "Invalid Key!"
      fi
    else
        echo "No SSH Key."
    fi
    if [ -n "$MYHOSTNAME" ]; then
        echo "Setting Hostname from Metadata Service to:" $MYHOSTNAME
        echo $MYHOSTNAME > /etc/myname
        /bin/hostname $MYHOSTNAME
    else
        echo "No Hostname."
    fi
    ;;

  deploy)
    rm /etc/random.seed
    rm /var/db/host.random
    rm /root/.ssh/known_hosts
    rm $USERHOME/.ssh/known_hosts
    chpass -a "root:*:0:0:daemon:0:0:Charlie &:/root:/bin/ksh"
    chpass -a "$USERACCOUNT:*:$USERUID:$USERGID:staff:0:0:$USERREALNAME:$USERHOME:$USERSHELL"
    if [ -f $MYPATH ]; then
      chmod +x $MYPATH
      echo "$MYPATH execute" >> /etc/rc.firsttime
      echo "$MYPATH has been deployed to /etc/rc.firsttime. System is ready for your cloud."
      echo "Please shut down your machine now."
      exit 0
    else
      echo "$MYPATH not found in the correct place. Installing it for you now and re-deploying..."
      cp $0 $MYPATH
      chmod +x $MYPATH
      $MYPATH deploy
      exit 0
    fi
  ;;

  *)
    echo "Usage: $0 {deploy|execute}"
    exit 2
  ;;
esac

exit 0
# release 1.0