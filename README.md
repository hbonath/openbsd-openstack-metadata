# OpenBSD Openstack Metadata

### Basic metadata script for deploying OpenBSD on Openstack Nova

To install, load your VM from OpenBSD install media with the defaults aside with the following exceptions:  

* Change the default console to com0? __YES__
* Which speed should com0 use? __115200__
* Set up a user? __*username*__
* Allow root ssh login? __prohibit-password__

After installation is complete, when asked to reboot, select "__S__" for Shell.  

* Chroot to newly installed system: `chroot /mnt`  
* Copy openbsd-metadata.sh script to /usr/local/bin and `chmod 755 /usr/local/bin/openbsd-metadata.sh`  
* Deploy script by running `/usr/local/bin/openbsd-metadata.sh deploy` prior to shutting down your system.  

Shut down your VM and upload to your Openstack cloud:  
Openstack Example: `openstack image create --container-format bare --disk-format qcow2 --min-disk 8 --min-ram 512 --property architecture='x86_64' --property os_distro='openbsd' --property os_version='6.5' --file ./openbsd-openstack.qcow2 openbsd-65`